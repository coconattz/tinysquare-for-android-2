package jp.natzsoft.tinysquare.db;

import java.util.List;
import jp.natzsoft.tinysquare.db.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table VENUE_CATEGORY.
 */
public class VenueCategory {

    private Long id;
    /** Not-null value. */
    private String categoryId;
    /** Not-null value. */
    private String name;
    private String prefix;
    private String suffix;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient VenueCategoryDao myDao;

    private List<VenueData> venues;

    public VenueCategory() {
    }

    public VenueCategory(Long id) {
        this.id = id;
    }

    public VenueCategory(Long id, String categoryId, String name, String prefix, String suffix) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getVenueCategoryDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /** Not-null value. */
    public String getCategoryId() {
        return categoryId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /** Not-null value. */
    public String getName() {
        return name;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<VenueData> getVenues() {
        if (venues == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            VenueDataDao targetDao = daoSession.getVenueDataDao();
            List<VenueData> venuesNew = targetDao._queryVenueCategory_Venues(id);
            synchronized (this) {
                if(venues == null) {
                    venues = venuesNew;
                }
            }
        }
        return venues;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetVenues() {
        venues = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
