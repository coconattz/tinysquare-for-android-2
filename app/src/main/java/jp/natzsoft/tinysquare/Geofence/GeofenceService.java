package jp.natzsoft.tinysquare.Geofence;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import jp.natzsoft.tinysquare.TinysquareService;

/**
 * Created by natsume on 2015/03/30.
 */
public class GeofenceService extends IntentService {

    private static final String TAG = "GeofenceService";

    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public GeofenceService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate [in]");
        super.onCreate();
        Log.v(TAG, "onCreate [out]");
    }

    /**
     * Handles incoming intents.
     * @param intent sent by Location Services. This Intent is provided to Location
     *               Services (inside a PendingIntent) when addGeofences() is called.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.v(TAG, "onHandleIntent [in]");

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type, location and geofences.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        final Location location = geofencingEvent.getTriggeringLocation();
        List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            for (Geofence geofence : geofences) {
                Intent newIntent = new Intent(GeofenceService.this, TinysquareService.class);
                newIntent.setAction(TinysquareService.INTENT_ACTION_ENTER_EXIT_GEOFENCE);
                newIntent.putExtra("id", geofence.getRequestId());
                newIntent.putExtra("transition", geofenceTransition);
                newIntent.putExtra("latitude", location.getLatitude());
                newIntent.putExtra("longitude", location.getLongitude());
                startService(newIntent);
                Log.i(TAG, "startService " + newIntent);
            }
        } else {
            // Log the error.
            Log.e(TAG, "Geofence transition error: invalid transition type %1$d");
        }

        Log.v(TAG, "onHandleIntent [out]");
    }
}
