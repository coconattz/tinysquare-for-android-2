package jp.natzsoft.tinysquare.Geofence;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.Model.Constants;
import jp.natzsoft.tinysquare.Model.DBAccessor;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.db.VenueData;
import jp.natzsoft.tinysquare.db.VenueDataDao;

/**
 * Created by natsume on 2015/04/08.
 */
public class GeofenceManager {

    private static final String TAG = "GeofenceManager";

    private static final String PREFERENCE_KEY_AUTO_CHECK_IN = "key_switch_auto_check_in";

    protected Context mContext;

    private boolean mGeofencesStarted;
    private boolean mGeofencesEnabled;

    protected GoogleApiClient mGoogleApiClient;
    protected ArrayList<Geofence> mGeofenceList;

    private PendingIntent mGeofencePendingIntent;

    private SharedPreferences mSharedPreferences;

    public interface GeofenceManagerListener {
        void updateGeofenceService(final boolean start);
    }
    protected GeofenceManagerListener listener;
    public void setGeofenceManagerListener(GeofenceManagerListener listener) {
        this.listener = listener;
    }

    public GeofenceManager(Context context, GoogleApiClient client) {
        mContext = context;
        mGoogleApiClient = client;
        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<Geofence>();
        // Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
        mGeofencePendingIntent = null;

        // Retrieve an instance of the SharedPreferences object.
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        // Get the value of mGeofencesAdded from SharedPreferences. Set to false as a default.
        mGeofencesStarted = false;

        mGeofencesEnabled = mSharedPreferences.getBoolean(PREFERENCE_KEY_AUTO_CHECK_IN, false);

        // Init geofences list.
        init();
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    public void updateGeofenceService() {
        if (mGeofencesEnabled && mGeofenceList.size() > 0) {
            startGeofencesService();
        } else {
            stopGeofencesService();
        }
    }

    public void updateGeofenceService(boolean start) {
        mGeofencesEnabled = start;
        updateGeofenceService();
    }

    /**
     * Adds geofences, which sets alerts to be notified when the device enters or exits one of the
     * specified geofences. Handles the success or failure results returned by addGeofences().
     */
    public void startGeofencesService() {
        if (!mGeofencesEnabled) {
            return;
        }
        if (!mGoogleApiClient.isConnected()) {
            sendNotification(mContext.getString(R.string.app_name), mContext.getString(R.string.not_connected));
            return;
        }
        if (mGeofenceList.size() == 0) {
            sendNotification(mContext.getString(R.string.app_name), mContext.getString(R.string.geofence_no_list));
            return;
        }

        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    // The GeofenceRequest object.
                    getGeofencingRequest(),
                    // A pending intent that that is reused when calling removeGeofences(). This
                    // pending intent is used to generate an intent when a matched geofence
                    // transition is observed.
                    getGeofencePendingIntent()
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        updateGeofenceServiceStatus(true);
                    } else {
                        // Get the status code for the error and log it using a user-friendly message.
                        String errorMessage = GeofenceErrorMessages.getErrorString(mContext,
                                status.getStatusCode());
                        Log.e(TAG, errorMessage);
                    }
                }
            }); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            logSecurityException(securityException);
        }
    }

    /**
     * Removes geofences, which stops further notifications when the device enters or exits
     * previously registered geofences.
     */
    public void stopGeofencesService() {
        if (!mGoogleApiClient.isConnected()) {
            sendNotification(mContext.getString(R.string.app_name), mContext.getString(R.string.not_connected));
            return;
        }
        try {
            // Remove geofences.
            LocationServices.GeofencingApi.removeGeofences(
                    mGoogleApiClient,
                    // This is the same pending intent that was used in addGeofences().
                    getGeofencePendingIntent()
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        updateGeofenceServiceStatus(false);
                    } else {
                        // Get the status code for the error and log it using a user-friendly message.
                        String errorMessage = GeofenceErrorMessages.getErrorString(mContext,
                                status.getStatusCode());
                        Log.e(TAG, errorMessage);
                    }
                }
            }); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            logSecurityException(securityException);
        }
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }

    /**
     * Runs when the result of calling addGeofences() and removeGeofences() becomes available.
     * Either method can complete successfully or with an error.
     *
     * Since this activity implements the {@link ResultCallback} interface, we are required to
     * define this method.
     *
     * @param status The Status returned through a PendingIntent when addGeofences() or
     *               removeGeofences() get called.
     */
    private void updateGeofenceServiceStatus(boolean status) {
        // TODO : listener
        // Update state and save in shared preferences.
        if (mGeofencesStarted != status) {
            mGeofencesStarted = status;
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putBoolean(Constants.GEOFENCES_STARTED_KEY, mGeofencesStarted);
            editor.commit();

            if (listener != null) {
                listener.updateGeofenceService(status);
            }
        }
    }

    private void init() {
        mGeofenceList.clear();
        final DBAccessor dbAccessor = new DBAccessor(mContext);
        VenueDataDao dao = dbAccessor.getVenueDataDao();
        List<VenueData> venues = dao.loadAll();
        for (VenueData venue : venues) {
            Geofence geofence = new Geofence.Builder()
                    // Set the request ID of the geofence = venueId.
                    .setRequestId(venue.getVenueId())

                            // Set the circular region of this geofence.
                    .setCircularRegion(
                            venue.getLatitude(),
                            venue.getLongitude(),
                            (venue.getRadius() + 1) * 100
                    )

                            // Set the expiration duration of the geofence.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                            // Set the transition types of interest.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build();
            mGeofenceList.add(geofence);
        }
    }

    public Geofence getGeofence(String id) {
        for (Geofence geofence : mGeofenceList ) {
            if (geofence.getRequestId().equals(id)) {
                // This venue is already added.
                return geofence;
            }
        }
        return null;
    }

    public boolean isAddedGeofence(String id) {
        for (Geofence geofence : mGeofenceList ) {
            if (geofence.getRequestId().equals(id)) {
                // This venue is already added.
                return true;
            }
        }
        return false;
    }
    /**
     *
     * @param geofence
     */
    public void addGeofence(Geofence geofence) {
        Geofence oldGeofence = getGeofence(geofence.getRequestId());
        if (oldGeofence != null) {
            // This venue is already added. Replace new Geofence.
            mGeofenceList.remove(oldGeofence);
        }
        mGeofenceList.add(geofence);
    }

    public void removeGeofence(String requestId) {
        Geofence geofence = getGeofence(requestId);
        if (geofence != null) {
            mGeofenceList.remove(geofence);
        }

        // Update geofence service.
        if (mGeofenceList.size() > 0) {
            // Update geofence service.
            startGeofencesService();
        } else {
            // Stop geofence service.
            stopGeofencesService();
        }
    }

    public void initializeGeofences() {
        init();
        if (mGoogleApiClient.isConnected()) {
            updateGeofenceService();
        }
    }

    public void updateAllGeofences() {
        init();
        updateGeofenceService();
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(mContext, GeofenceService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    static int notificationId = 0;
    /**
     * Posts a notification in the notification bar when a transition is detected.
     * If the user clicks the notification, control goes to the MainActivity.
     */
    private void sendNotification(String title, String text) {
        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(mContext, MainActivity.class);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        Notification.Builder builder = new Notification.Builder(mContext);

        // Define the notification settings.
        builder.setContentIntent(notificationPendingIntent)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(title)
                .setContentText(text);

        // Set color (API 21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setColor(mContext.getResources().getColor(R.color.primary))
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setCategory(Notification.CATEGORY_SERVICE);
        }

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification
        mNotificationManager.notify(notificationId, builder.build());
    }
}
