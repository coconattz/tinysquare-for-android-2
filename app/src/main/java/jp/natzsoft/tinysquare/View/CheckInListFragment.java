package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.condesales.criterias.VenuesCriteria;
import br.com.condesales.listeners.AccessTokenRequestListener;
import br.com.condesales.listeners.FoursquareVenuesRequestListener;
import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.Model.Constants;
import jp.natzsoft.tinysquare.R;

/**
 * Created by natsume on 2015/03/26.
 */
public class CheckInListFragment extends Fragment {

    private static final String TAG = "CheckInListFragment";

    private MainActivity mActivity;
    private ArrayList<Venue> mVenues;

    private Location mLastLocation;

    @Override
    public void onAttach(Activity activity) {
        Log.v(TAG, "onAttach [in]");
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
        Log.v(TAG, "onAttach [out]");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate [in]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.v(TAG, "onCreate [out]");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(TAG, "onCreateView [in]");
        View view = inflater.inflate(R.layout.fragment_list_check_in, container, false);

        ListView listView = (ListView)view.findViewById(R.id.venueList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showCheckinDetail(view, i);
            }
        });
        if (mVenues != null) {
            CheckInListAdapter adapter = new CheckInListAdapter(getActivity(), 0, mVenues);
            listView.setAdapter(adapter);
        }
        Log.v(TAG, "onCreateView [out]");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("チェックイン一覧");
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume [in]");
        mActivity.setTitle(mActivity.getString(R.string.app_name));
        startLocationService();
        super.onResume();
        Log.v(TAG, "onResume [out]");
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause [in]");
        stopLocationService();
        super.onPause();
        Log.v(TAG, "onPause [out]");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_checkin_list, menu);

        // Get SearchView
        final MenuItem searchItem = menu.findItem(R.id.action_serach);
        final MenuItem refreshItem = menu.findItem(R.id.action_refresh);
        final MenuItem autoListItem = menu.findItem(R.id.action_list_auto_checkin);
        final MenuItem settingsItem = menu.findItem(R.id.action_show_settings);
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                refreshItem.setVisible(false);
                autoListItem.setVisible(false);
                settingsItem.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                refreshItem.setVisible(true);
                autoListItem.setVisible(true);
                settingsItem.setVisible(true);
                requestVenuesNearby(mActivity.getLastLocation(), null);
                return true;
            }
        });
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                requestVenuesNearby(mActivity.getLastLocation(), query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                requestVenuesNearby(mActivity.getLastLocation(), null);
                return true;
            case R.id.action_list_auto_checkin:
                showAutoCheckinList();
                return true;
            case R.id.action_show_settings:
                showSettingsFragment();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTransition(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionSet enter = new TransitionSet();
            enter.addTransition(new Slide());
            fragment.setEnterTransition(enter);
            TransitionSet exit = new TransitionSet();
            exit.addTransition(new Fade());
            fragment.setExitTransition(exit);
        }
    }

    private void showCheckinDetail(View view, int i) {
        Venue venue = mVenues.get(i);
        CheckInDetailFragment fragment = new CheckInDetailFragment();
        fragment.setVenue(venue);
        setTransition(fragment);
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "venueDetail")
                .addToBackStack(null)
                .commit();
    }

    private void showAutoCheckinList() {
        AutoCheckInListFragment fragment = new AutoCheckInListFragment();
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "autoCheckinList")
                .addToBackStack(null)
                .commit();
    }

    private void showSettingsFragment() {
        SettingsFragment fragment = new SettingsFragment();
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "settings")
                .addToBackStack(null)
                .commit();
    }

    public void setVenueList(ArrayList<Venue> venues) {
        mVenues = venues;
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        ViewGroup viewGroup = (ViewGroup)getView();
        View view = inflater.inflate(R.layout.fragment_list_check_in, viewGroup);
        ListView listView = (ListView)view.findViewById(R.id.venueList);
        if (mVenues != null) {
            CheckInListAdapter adapter = new CheckInListAdapter(getActivity(), 0, mVenues);
            listView.setAdapter(adapter);
        } else {
            listView.setAdapter(null);
        }
    }

    private void startLocationService() {
        mActivity.setLocationListener(locationListener);
        if (mActivity.getEasyFoursquare().hasAccessToken()) {
            mActivity.startUpdateLocation();
        } else {
            requestAccess();
        }
    }

    private void stopLocationService() {
        mActivity.setLocationListener(null);
        mActivity.stopUpdateLocation();
    }

    private boolean requestedVenues = false;
    private void requestVenuesNearby(Location location, String query){
        Log.v(TAG, "requestVenuesNearby [in]");

        if (requestedVenues) {
            return;
        }
        if (location == null) {
            return;
        }

        setVenueList(null);

        mActivity.showProgressBar();

        VenuesCriteria criteria = new VenuesCriteria();
        criteria.setLocation(location);
        criteria.setQuantity(20);
        if (query != null) {
            criteria.setQuery(query);
            criteria.setRadius(5000);
        }

        mActivity.getEasyFoursquare().getVenuesNearby(venuesRequestListener, criteria);
        requestedVenues = true;

        Log.v(TAG, "requestVenuesNearby [out]");
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            float distance = 1000;
            if (mLastLocation != null) {
                distance = mLastLocation.distanceTo(location);
            }
            if (distance > 100) {
                mLastLocation = location;
                requestVenuesNearby(location, null);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private boolean requestedAccessToken = false;
    private void requestAccess() {
        if (!requestedAccessToken) {
            requestedAccessToken = true;
            mActivity.getEasyFoursquare().requestAccess(accessTokenRequestListener);
        }
    }

    private AccessTokenRequestListener accessTokenRequestListener = new AccessTokenRequestListener() {
        @Override
        public void onAccessGrant(String accessToken) {
            Log.v(TAG, "onAccessGrant [in]");
            requestedAccessToken = false;
            // Save access token.
            SharedPreferences preferences = mActivity.getDefaultSharedPreferences();
            preferences.edit().putString(Constants.ACCESS_TOKEN, accessToken).commit();
            // Start Location Service.
            mActivity.startUpdateLocation();
            Log.v(TAG, "onAccessGrant [out]");
        }

        @Override
        public void onError(String errorMsg) {
            Log.v(TAG, "onError [in] : " + errorMsg);
            requestedAccessToken = false;
            // Do something with the error message
            Toast.makeText(mActivity, errorMsg, Toast.LENGTH_LONG).show();
            Log.v(TAG, "onError [out]");
        }
    };

    private FoursquareVenuesRequestListener venuesRequestListener = new FoursquareVenuesRequestListener() {
        @Override
        public void onVenuesFetched(ArrayList<Venue> venues) {
            Log.v(TAG, "onVenuesFetched [in]");
            requestedVenues = false;
            mActivity.hideProgressBar();
            setVenueList(venues);
            Log.v(TAG, "onVenuesFetched [out]");
        }

        @Override
        public void onError(String errorMsg) {
            Log.v(TAG, "onError [in] : " + errorMsg);
            mActivity.hideProgressBar();
            requestedVenues = false;
            Log.v(TAG, "onError [out]");
        }
    };
}
