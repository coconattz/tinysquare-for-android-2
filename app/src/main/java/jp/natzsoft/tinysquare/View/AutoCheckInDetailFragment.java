package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.db.VenueCategory;
import jp.natzsoft.tinysquare.db.VenueData;
import jp.natzsoft.tinysquare.db.VenueDataDao;

/**
 * Created by natsume on 2015/03/26.
 */
public class AutoCheckInDetailFragment extends Fragment {

    private static final String TAG = "AutoCheckInDetail";

    private static final String KEY_SHOUT = "shout";
    private static final String KEY_BROADCAST = "broadcast";
    private static final String KEY_TWITTER = "twitter";
    private static final String KEY_FACEBOOK = "facebook";
    private static final String KEY_RADIUS = "radius";

    private MainActivity mActivity;
    private VenueData mVenue;
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    private EditText mEditText;

    private boolean mIsPrivate;
    private boolean mIsTwitter;
    private boolean mIsFacebook;
    private Integer mRadius;

    public void setVenue(VenueData venue) {
        mVenue = venue;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate [in]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.v(TAG, "onCreate [out]");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_auto_check_in, container, false);

        layoutSubViews(view, savedInstanceState);

        return view;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        ViewGroup viewGroup = (ViewGroup)getView();
        viewGroup.removeAllViewsInLayout();
        View view = inflater.inflate(R.layout.fragment_detail_auto_check_in, viewGroup);
        Bundle bundle = new Bundle();
        onSaveInstanceState(bundle);
        layoutSubViews(view, bundle);
    }

    private void layoutSubViews(View view, Bundle savedInstanceState) {
        // Icon
        final ImageView iconView = (ImageView)view.findViewById(R.id.detailIcon);
        if (mVenue.getVenueCategory() != null) {
            VenueCategory category = mVenue.getVenueCategory();
            float scale = mActivity.getResources().getDisplayMetrics().density;
            Bitmap bmp = mActivity.getBmpMap().get(VenueAccessor.getSavedFileName(category.getPrefix(), category.getSuffix(), scale, true));
            if (bmp != null) {
                iconView.setImageBitmap(bmp);
            } else {
                iconView.setImageResource(R.drawable.ic_none);
            }
        } else {
            iconView.setImageResource(R.drawable.ic_none);
        }

        // Venue name
        final TextView name = (TextView)view.findViewById(R.id.detailName);
        name.setText(mVenue.getName());

        // Sub title
        final TextView subTitle = (TextView)view.findViewById(R.id.detailSubTitle);
        subTitle.setText(VenueAccessor.getLastCheckin(mActivity, mVenue));

        // Facebook
        mIsFacebook = VenueAccessor.isFacebook(mVenue.getBroadcast());
        if (savedInstanceState != null) {
            mIsFacebook = savedInstanceState.getBoolean(KEY_FACEBOOK);
        }
        final ImageButton facebookBtn = (ImageButton)view.findViewById(R.id.detailFbButton);
        facebookBtn.setActivated(mIsFacebook);
        facebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsFacebook = !mIsFacebook;
                updateFacebookBtn(facebookBtn);
            }
        });

        // Twitter
        mIsTwitter = VenueAccessor.isTwitter(mVenue.getBroadcast());
        if (savedInstanceState != null) {
            mIsTwitter = savedInstanceState.getBoolean(KEY_TWITTER);
        }
        final ImageButton twitterBtn = (ImageButton)view.findViewById(R.id.detailTwtButton);
        twitterBtn.setActivated(mIsTwitter);
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsTwitter = !mIsTwitter;
                updateTwitterBtn(twitterBtn);
            }
        });

        // Public / Private
        mIsPrivate = VenueAccessor.isPrivate(mVenue.getBroadcast());
        if (savedInstanceState != null) {
            mIsPrivate = savedInstanceState.getBoolean(KEY_BROADCAST);
        }
        final TextView broadcastText = (TextView)view.findViewById(R.id.detailPublicText);
        final ImageButton broadcast = (ImageButton)view.findViewById(R.id.detailPublicButton);
        broadcast.setActivated(mIsPrivate);
        broadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsPrivate = !mIsPrivate;
                updateBroadcast(broadcast, broadcastText, twitterBtn, facebookBtn);
            }
        });
        updateBroadcast(broadcast, broadcastText, twitterBtn, facebookBtn);

        // Radius
        mRadius = mVenue.getRadius();
        if (savedInstanceState != null) {
            mRadius = savedInstanceState.getInt(KEY_RADIUS);
        }
        final TextView distanceText = (TextView)view.findViewById(R.id.distanceText);
        distanceText.setText(getDistanceText(mRadius));
        final SeekBar seekBar = (SeekBar)view.findViewById(R.id.seekBar);
        seekBar.setProgress(mRadius);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mRadius = progress;
                distanceText.setText(getDistanceText(mRadius));
                updateMapOptions();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Google Map
        mMapView = (MapView)view.findViewById(R.id.detailMapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                if (mGoogleMap != null) {
                    mGoogleMap.setMyLocationEnabled(true);
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
                    mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
                    LatLng latLng = new LatLng(mVenue.getLatitude(), mVenue.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15.0f);
                    mGoogleMap.moveCamera(cameraUpdate);
                    updateMapOptions();
                }
            }
        });

        // Shout
        String shout = mVenue.getShout();
        if (savedInstanceState != null) {
            shout = savedInstanceState.getString(KEY_SHOUT);
        }
        mEditText = (EditText)view.findViewById(R.id.detailComment);
        mEditText.setText(shout);
        mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean ret = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mMapView.onResume();
                    InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    ret = true;
                }
                return ret;
            }
        });
        mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mMapView.onPause();
                } else {
                    mMapView.onResume();
                    InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        // Delete
        final Button deleteButton = (Button)view.findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_BROADCAST, mIsPrivate);
        outState.putBoolean(KEY_TWITTER, mIsTwitter);
        outState.putBoolean(KEY_FACEBOOK, mIsFacebook);
        outState.putInt(KEY_RADIUS, mRadius);
        outState.putString(KEY_SHOUT, mEditText.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("自動チェックイン詳細");
    }

    @Override
    public void onResume() {
        mActivity.setTitle(mActivity.getString(R.string.auto_check_in));
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_auto_checkin_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateBroadcast(final ImageButton broadcast, final TextView broadcastText, final ImageButton twitterBtn, final ImageButton facebookBtn) {
        if (mIsPrivate) {
            broadcast.setActivated(true);
            broadcastText.setText(R.string.detail_private);
            facebookBtn.setVisibility(View.INVISIBLE);
            twitterBtn.setVisibility(View.INVISIBLE);
        } else {
            broadcast.setActivated(false);
            broadcastText.setText(R.string.detail_public);
            facebookBtn.setVisibility(View.VISIBLE);
            twitterBtn.setVisibility(View.VISIBLE);
        }
    }

    private void updateTwitterBtn(final ImageButton twitterBtn) {
        if (mIsTwitter) {
            twitterBtn.setActivated(true);
        } else {
            twitterBtn.setActivated(false);
        }
    }

    private void updateFacebookBtn(final ImageButton facebookBtn) {
        if (mIsFacebook) {
            facebookBtn.setActivated(true);
        } else {
            facebookBtn.setActivated(false);
        }
    }

    private void updateMapOptions() {
        if (mGoogleMap != null) {
            LatLng latLng = new LatLng(mVenue.getLatitude(), mVenue.getLongitude());
            mGoogleMap.clear();
            mGoogleMap.addMarker(new MarkerOptions()
                            .position(latLng)
            );
            mGoogleMap.addCircle(new CircleOptions()
                            .center(latLng)
                            .radius((mRadius + 1) * 100.0)
                            .fillColor(getResources().getColor(R.color.circle_primary))
                            .strokeColor(getResources().getColor(R.color.circle_primary_dark))
                            .strokeWidth(2f)
            );
        }
    }

    private String getDistanceText(Integer progress) {
        return String.valueOf((progress + 1) * 100)  + "m";
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(mVenue.getName())
                .setMessage(mActivity.getString(R.string.confirm_delete))
                .setPositiveButton(mActivity.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .setNegativeButton(mActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes
                        delete();
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void delete() {
        // Delete from Geofence list
        mActivity.removeGeofence(mVenue.getVenueId());

        // Delete from Database
        VenueDataDao dao = mActivity.getDBAccessor().getVenueDataDao();
        dao.delete(mVenue);

        // Remove fragment
        getFragmentManager().popBackStack();
    }

    private void save() {
        mEditText.clearFocus();

        mVenue.setBroadcast(VenueAccessor.getBroadcast(mIsPrivate, mIsTwitter, mIsFacebook));
        mVenue.setShout(mEditText.getText().toString());
        mVenue.setRadius(mRadius);
        mVenue.update();

        // Update geofence data.
        mActivity.updateGeofence(mVenue);

        // Remove fragment
        getFragmentManager().popBackStack();
    }

}
