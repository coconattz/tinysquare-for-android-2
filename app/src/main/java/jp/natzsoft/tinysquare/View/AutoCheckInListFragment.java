package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.condesales.criterias.CheckInCriteria;
import br.com.condesales.listeners.CheckInListener;
import br.com.condesales.models.Checkin;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.Utils.ModelUtils;
import jp.natzsoft.tinysquare.db.VenueData;
import jp.natzsoft.tinysquare.db.VenueDataDao;

/**
 * Created by nattz on 2015/04/14.
 */
public class AutoCheckInListFragment extends Fragment {

    private static final String TAG = "AutoCheckInListFragment";

    private MainActivity mActivity;
    private ArrayList<VenueData> mVenues;
    private ListView mListView;

    @Override
    public void onAttach(Activity activity) {
        Log.v(TAG, "onAttach [in]");
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
        Log.v(TAG, "onAttach [out]");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate [in]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.v(TAG, "onCreate [out]");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(TAG, "onCreateView [in]");
        View view = inflater.inflate(R.layout.fragment_list_check_in, container, false);

        ListView listView = (ListView)view.findViewById(R.id.venueList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showCheckinDetail(i);
            }
        });
        mVenues = new ArrayList<VenueData>(mActivity.getDBAccessor().getVenueDataDao().loadAll());
        AutoCheckInListAdapter adapter = new AutoCheckInListAdapter(getActivity(), 0, mVenues);
        listView.setAdapter(adapter);
        mListView = listView;

        registerForContextMenu(listView);

        Log.v(TAG, "onCreateView [out]");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("自動チェックイン一覧");
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume [in]");
        mActivity.setTitle(mActivity.getString(R.string.auto_check_in));
        super.onResume();
        Log.v(TAG, "onResume [out]");
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause [in]");
        super.onPause();
        Log.v(TAG, "onPause [out]");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_auto_checkin_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret = true;
        switch (item.getItemId()) {
            case R.id.action_add_auto_checkin_list:
                showSelectVenueList();
                break;
            default:
                ret = super.onOptionsItemSelected(item);
                break;
        }
        return ret;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);

        // Get Venue Data
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        VenueData venue = mVenues.get(info.position);

        // Set Context Menu
        menu.setHeaderTitle(venue.getName());
        MenuInflater inflater = this.getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu_auto_checkin_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        boolean ret = true;
        AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
        VenueData venue = mVenues.get(info.position);

        switch(item.getItemId()){
            case R.id.action_checkin_list:
                checkin(venue);
                break;
            case R.id.action_delete_auto_checkin_list:
                // Delete Venue
                deleteVenue(venue);
                break;
            default:
                ret = super.onContextItemSelected(item);
                break;
        }
        return ret;
    }

    /**
     * Check-In to the Venue.
     * @param venue Venue Data
     */
    private void checkin(VenueData venue) {

        mActivity.showProgressBar();

        CheckInCriteria criteria = new CheckInCriteria();
        criteria.setVenueId(venue.getVenueId());
        criteria.setLocation(mActivity.getLastLocation());
        criteria.setShout(venue.getShout());
        criteria.setBroadcast(VenueAccessor.getBroadcastType(venue.getBroadcast()));

        mActivity.getEasyFoursquare().checkIn(checkInListener, criteria);
    }

    private CheckInListener checkInListener = new CheckInListener() {
        @Override
        public void onCheckInDone(Checkin checkin) {
            mActivity.hideProgressBar();
            Toast.makeText(mActivity, R.string.complete_checkin, Toast.LENGTH_SHORT).show();
            mActivity.getDBAccessor().updateDate(checkin.getVenue().getId());

            String name = "";
            if (checkin.getVenue().getName() != null) {
                name = checkin.getVenue().getName();
            }
            mActivity.sendEvent("Check-in", "手動チェックイン完了(登録済み)", name);
        }

        @Override
        public void onError(String errorMsg) {
            mActivity.hideProgressBar();
            Toast.makeText(mActivity, errorMsg, Toast.LENGTH_LONG).show();
            mActivity.sendEvent("Error", "Check-in", errorMsg);
        }
    };

    /**
     * Delete Venue Data from Database and ArrayAdapter.
     * @param venue Venue Data
     */
    private void deleteVenue(VenueData venue) {
        // Delete from Geofence list
        mActivity.removeGeofence(venue.getVenueId());

        // Delete from Database
        VenueDataDao dao = mActivity.getDBAccessor().getVenueDataDao();
        dao.delete(venue);

        // Delete from Adapter (= mVenues)
        AutoCheckInListAdapter adapter = (AutoCheckInListAdapter)mListView.getAdapter();
        adapter.remove(venue);
    }

    private void setTransition(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionSet ts = new TransitionSet();
            ts.addTransition(new Fade());
            ts.addTransition(new Slide());
            fragment.setEnterTransition(ts);
        }
    }

    private void showCheckinDetail(int i) {
        VenueData venue = mVenues.get(i);
        AutoCheckInDetailFragment fragment = new AutoCheckInDetailFragment();
        fragment.setVenue(venue);
        setTransition(fragment);
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "autoVenueDetail")
                .addToBackStack(null)
                .commit();
    }

    private void showSelectVenueList() {
        if (ModelUtils.isLiteVer(mActivity)) {
            Toast.makeText(mActivity, R.string.toast_lite_version, Toast.LENGTH_LONG).show();
            return;
        }
        SelectVenueListFragment fragment = new SelectVenueListFragment();
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "selectVenueList")
                .addToBackStack(null)
                .commit();
    }
}
