package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.Toast;

import br.com.condesales.listeners.AccessTokenRequestListener;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.Model.Constants;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.TinysquareService;

/**
 * Created by natsume on 2015/04/17.
 */
public class SettingsFragment extends PreferenceFragment {

    private static final String TAG = "SettingsFragment";

    private MainActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        Log.v(TAG, "onAttach [in]");
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
        Log.v(TAG, "onAttach [out]");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate [in]");
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.main_preference);

        Preference login = findPreference(getActivity().getString(R.string.key_logout));
        login.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                logout();
                reloadSummary();
                login();
                return false;
            }
        });

        Log.v(TAG, "onCreate [out]");
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("設定");
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume [in]");
        mActivity.setTitle(mActivity.getString(R.string.menu_action_settings));
        super.onResume();
        reloadSummary();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
        Log.v(TAG, "onResume [out]");
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause [in]");
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
        Log.v(TAG, "onPause [out]");
    }

    private void reloadSummary() {
        ListAdapter adapter = getPreferenceScreen().getRootAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            Object item = adapter.getItem(i);
            if (item instanceof ListPreference) {
                ListPreference preference = (ListPreference) item;
                String prefix = null;
                if (preference.getKey().equals(getActivity().getString(R.string.key_list_time_to_check_in))) {
                    prefix = getActivity().getString(R.string.summary_time_to_check_in);
                } else if (preference.getKey().equals(getActivity().getString(R.string.key_list_next_check_in_time))) {
                    prefix = getActivity().getString(R.string.summary_next_check_in_time);
                }
                preference.setSummary(preference.getEntry() == null ? prefix : preference.getEntry() + "\n\n" + prefix);
            } else if (item instanceof Preference) {
                Preference preference = (Preference) item;
                if (preference.getKey() != null && preference.getKey().equals(getActivity().getString(R.string.key_logout))) {
                    if (mActivity.getEasyFoursquare().hasAccessToken()) {
                        preference.setTitle(getActivity().getString(R.string.logout));
                    } else {
                        preference.setTitle(getActivity().getString(R.string.login));
                    }
                }
            }
        }
    }

    private SharedPreferences.OnSharedPreferenceChangeListener listener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    if (key.equals(getActivity().getString(R.string.key_switch_auto_check_in))) {
                        Boolean geofence = sharedPreferences.getBoolean(getActivity().getString(R.string.key_switch_auto_check_in), true);
                        // Send Intent to TinysquareService
                        Intent newIntent = new Intent(mActivity, TinysquareService.class);
                        newIntent.setAction(TinysquareService.INTENT_ACTION_ON_OFF_GEOFENCE);
                        newIntent.putExtra("enabled", geofence);
                        mActivity.startService(newIntent);
                    }
                    reloadSummary();
                }
            };

    private void login() {
        Log.v(TAG, "login [in]");
        mActivity.getEasyFoursquare().requestAccess(accessTokenRequestListener);
        Log.v(TAG, "login [out]");
    }

    private void logout() {
        mActivity.getEasyFoursquare().revokeAccess();
    }

    private AccessTokenRequestListener accessTokenRequestListener = new AccessTokenRequestListener() {
        @Override
        public void onAccessGrant(String accessToken) {
            Log.v(TAG, "onAccessGrant [in]");
            // Save access token.
            SharedPreferences preferences = mActivity.getDefaultSharedPreferences();
            preferences.edit().putString(Constants.ACCESS_TOKEN, accessToken).commit();

            reloadSummary();
            Log.v(TAG, "onAccessGrant [out]");
        }

        @Override
        public void onError(String errorMsg) {
            Log.v(TAG, "onError [in] : " + errorMsg);
            // Do something with the error message
            Toast.makeText(mActivity, errorMsg, Toast.LENGTH_LONG).show();
            Log.v(TAG, "onError [out]");
        }
    };
}
