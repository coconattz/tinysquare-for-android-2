package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.condesales.criterias.VenuesCriteria;
import br.com.condesales.listeners.FoursquareVenuesRequestListener;
import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.TinysquareService;

/**
 * Created by nattz on 2015/05/06.
 */
public class SelectVenueListFragment extends Fragment {


    private static final String TAG = "SelectVenueListFragment";

    private MainActivity mActivity;
    private ArrayList<Venue> mVenues;

    private ImageButton mFacebookBtn;
    private ImageButton mTwitterBtn;
    private ImageButton mBroadcastBtn;
    private ListView mListView;
    private MenuItem mSaveMenuItem;

    @Override
    public void onAttach(Activity activity) {
        Log.v(TAG, "onAttach [in]");
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
        Log.v(TAG, "onAttach [out]");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate [in]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        requestVenuesNearby(mActivity.getLastLocation(), null);
        Log.v(TAG, "onCreate [out]");
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(TAG, "onCreateView [in]");
        View view = inflater.inflate(R.layout.fragment_list_select_venue, container, false);

        // Facebook
        mFacebookBtn = (ImageButton)view.findViewById(R.id.detailFbButton);
        mFacebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFacebookBtn.isActivated()) {
                    mFacebookBtn.setActivated(false);
                } else {
                    mFacebookBtn.setActivated(true);
                }
            }
        });

        // Twitter
        mTwitterBtn = (ImageButton)view.findViewById(R.id.detailTwtButton);
        mTwitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTwitterBtn.isActivated()) {
                    mTwitterBtn.setActivated(false);
                } else {
                    mTwitterBtn.setActivated(true);
                }
            }
        });

        // Public / Private
        final TextView broadcastText = (TextView)view.findViewById(R.id.detailPublicText);
        mBroadcastBtn = (ImageButton)view.findViewById(R.id.detailPublicButton);
        mBroadcastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBroadcastBtn.isActivated()) {
                    mBroadcastBtn.setActivated(false);
                    broadcastText.setText(R.string.detail_public);
                    mFacebookBtn.setVisibility(View.VISIBLE);
                    mTwitterBtn.setVisibility(View.VISIBLE);
                } else {
                    mBroadcastBtn.setActivated(true);
                    broadcastText.setText(R.string.detail_private);
                    mFacebookBtn.setVisibility(View.INVISIBLE);
                    mTwitterBtn.setVisibility(View.INVISIBLE);
                }
            }
        });

        final ListView listView = (ListView)view.findViewById(R.id.venueList);
        mListView = listView;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
                checkBox.setChecked(!checkBox.isChecked());
                updateMenuItem();
            }
        });

        Log.v(TAG, "onCreateView [out]");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("Venue選択");
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume [in]");
        mActivity.setTitle(mActivity.getString(R.string.select_venues));
        super.onResume();
        Log.v(TAG, "onResume [out]");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_select_venue_list, menu);

        // Get SearchView
        final MenuItem searchItem = menu.findItem(R.id.action_serach);
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                requestVenuesNearby(mActivity.getLastLocation(), null);
                return true;
            }
        });
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                requestVenuesNearby(mActivity.getLastLocation(), query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mSaveMenuItem = menu.findItem(R.id.action_save);
        updateMenuItem();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateMenuItem() {
        if (mSaveMenuItem == null) {
            return;
        }
        if (isCheckedVenues()) {
            mSaveMenuItem.setEnabled(true);
            mSaveMenuItem.setVisible(true);
        } else {
            mSaveMenuItem.setEnabled(false);
            mSaveMenuItem.setVisible(false);
        }
    }

    public boolean isCheckedVenues() {
        boolean ret = false;
        SelectVenueListAdapter adapter = (SelectVenueListAdapter)mListView.getAdapter();
        if (adapter != null) {
            if (adapter.isCheckedVenues()) {
                ret = true;
            }
        }
        return ret;
    }

    public void setVenueList(ArrayList<Venue> venues) {
        if (venues != null) {
            ArrayList<Venue> newVenues = new ArrayList<Venue>();
            for (Venue venue : venues) {
                if (!mActivity.getDBAccessor().isExistedVenueDataById(venue.getId())) {
                    newVenues.add(venue);
                }
            }
            mVenues = newVenues;
        } else {
            mVenues = venues;
        }
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        ViewGroup viewGroup = (ViewGroup)getView();
        View view = inflater.inflate(R.layout.fragment_list_select_venue, viewGroup);
        ListView listView = (ListView)view.findViewById(R.id.venueList);
        if (mVenues != null) {
            SelectVenueListAdapter adapter = new SelectVenueListAdapter(mActivity, 0, mVenues);
            listView.setAdapter(adapter);
        } else {
            listView.setAdapter(null);
        }
    }

    private void save() {
        SelectVenueListAdapter adapter = (SelectVenueListAdapter)mListView.getAdapter();
        ArrayList<Boolean> checkedVenues = adapter.getCheckedVenues();
        for (int i = 0; i < mVenues.size(); i++) {
            if (checkedVenues.get(i)) {
                Venue venue = mVenues.get(i);
                mActivity.addGeofence(venue,
                        VenueAccessor.getBroadcast(mBroadcastBtn.isActivated(),
                                mTwitterBtn.isActivated(),
                                mFacebookBtn.isActivated()),
                        false);
                mActivity.sendEvent("Auto-Check-in", "一括登録", venue.getName());
            }
        }
        // Send Intent to TinysquareService
        Intent newIntent = new Intent(mActivity, TinysquareService.class);
        newIntent.setAction(TinysquareService.INTENT_ACTION_DATA_UPDATED);
        mActivity.startService(newIntent);

        getFragmentManager().popBackStack();
    }

    private boolean requestedVenues = false;
    private void requestVenuesNearby(Location location, String query){
        Log.v(TAG, "requestVenuesNearby [in]");

        if (requestedVenues) {
            return;
        }
        if (location == null) {
            return;
        }

        setVenueList(null);

        mActivity.showProgressBar();

        VenuesCriteria criteria = new VenuesCriteria();
        criteria.setLocation(location);
        criteria.setQuantity(50);
        if (query != null) {
            criteria.setQuery(query);
            criteria.setIntent(VenuesCriteria.VenuesCriteriaIntent.BROWSE);
            criteria.setRadius(100000);
        }

        mActivity.getEasyFoursquare().getVenuesNearby(venuesRequestListener, criteria);
        requestedVenues = true;

        Log.v(TAG, "requestVenuesNearby [out]");
    }

    private FoursquareVenuesRequestListener venuesRequestListener = new FoursquareVenuesRequestListener() {
        @Override
        public void onVenuesFetched(ArrayList<Venue> venues) {
            Log.v(TAG, "onVenuesFetched [in]");
            requestedVenues = false;
            mActivity.hideProgressBar();
            setVenueList(venues);
            Log.v(TAG, "onVenuesFetched [out]");
        }

        @Override
        public void onError(String errorMsg) {
            Log.v(TAG, "onError [in] : " + errorMsg);
            mActivity.hideProgressBar();
            requestedVenues = false;
            Log.v(TAG, "onError [out]");
        }
    };
}
