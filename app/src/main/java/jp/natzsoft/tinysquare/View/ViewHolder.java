package jp.natzsoft.tinysquare.View;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by nattz on 2015/04/14.
 */
public class ViewHolder {
    TextView title;
    TextView subTitle;
    ImageView icon;
}
