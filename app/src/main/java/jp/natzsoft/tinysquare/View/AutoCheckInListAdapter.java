package jp.natzsoft.tinysquare.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import br.com.condesales.listeners.ImageRequestListener;
import br.com.condesales.tasks.venues.CategoryImageRequest;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.db.VenueCategory;
import jp.natzsoft.tinysquare.db.VenueData;

/**
 * Created by nattz on 2015/04/14.
 */

public class AutoCheckInListAdapter extends ArrayAdapter<VenueData> {

    private static final String TAG = "AutoCheckInListAdapter";

    private LayoutInflater mLayoutInflater;
    private MainActivity mActivity;
    private HashMap<String, Bitmap> mBmpMap;

    public AutoCheckInListAdapter(Context context, int textViewResourceId, List<VenueData> objects) {
        super(context, textViewResourceId, objects);
        mActivity = (MainActivity)context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mBmpMap = mActivity.getBmpMap();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.v(TAG, "getView [in] : position = " + String.valueOf(position));

        ViewHolder holder;

        // Get Venue Data from position
        VenueData venue = (VenueData)getItem(position);

        // If convertView is null , make new object.
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_venue, null);
            holder = new ViewHolder();
            holder.title = (TextView)convertView.findViewById(R.id.name);
            holder.subTitle = (TextView)convertView.findViewById(R.id.venueId);
            holder.icon = (ImageView)convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        // Venue name
        holder.title.setText(venue.getName());

        // Last check-in date
        holder.subTitle.setText(VenueAccessor.getLastCheckin(getContext(), venue));

        // Icon ( if category is existed. )
        VenueCategory category = venue.getVenueCategory();
        if (category != null) {
            float scale = mActivity.getResources().getDisplayMetrics().density;
            Bitmap bmp = mBmpMap.get(VenueAccessor.getSavedFileName(category.getPrefix(), category.getSuffix(), scale, true));
            if (bmp != null) {
                holder.icon.setImageBitmap(bmp);
            } else {
                requestCategoryIcon(category.getPrefix(), category.getSuffix(), scale, position);
            }
        } else {
            holder.icon.setImageResource(R.drawable.ic_none);
        }
        Log.v(TAG, "getView [out]");
        return convertView;
    }

    private void requestCategoryIcon(final String prefix, final String suffix, float scale, int position) {
        String url = VenueAccessor.getIconUrl(prefix, suffix, scale, true);
        if (url != null) {
            CategoryImageRequest request = new CategoryImageRequest(mActivity, url, new ImageRequestListener() {
                @Override
                public void onImageFetched(Bitmap bmp, String fileName) {
                    Log.v(TAG, "onImageFetched [in] : name = " + fileName);
                    mBmpMap.put(fileName, bmp);
                    notifyDataSetChanged();
                    Log.v(TAG, "onImageFetched [out]");
                }

                @Override
                public void onError(String errorMsg) {

                }
            });
            request.execute();
        }
    }
}
