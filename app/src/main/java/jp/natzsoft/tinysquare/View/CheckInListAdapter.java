package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import br.com.condesales.listeners.ImageRequestListener;
import br.com.condesales.models.Category;
import br.com.condesales.models.Icon;
import br.com.condesales.models.Venue;
import br.com.condesales.tasks.venues.CategoryImageRequest;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;

/**
 * Created by natsume on 2015/03/23.
 */

public class CheckInListAdapter extends ArrayAdapter<Venue> {

    private static final String TAG = "CheckInListAdapter";

    private LayoutInflater mLayoutInflater;
    private MainActivity mActivity;
    private HashMap<String, Bitmap> mBmpMap;

    public CheckInListAdapter(Context context, int textViewResourceId, List<Venue> objects) {
        super(context, textViewResourceId, objects);
        mActivity = (MainActivity)context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mBmpMap = mActivity.getBmpMap();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        Log.v(TAG, "getView [in] : position = " + String.valueOf(position));

        ViewHolder holder;

        // 特定の行(position)のデータを得る
        Venue venue = (Venue)getItem(position);

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_venue, null);
            holder = new ViewHolder();
            holder.title = (TextView)convertView.findViewById(R.id.name);
            holder.subTitle = (TextView)convertView.findViewById(R.id.venueId);
            holder.icon = (ImageView)convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        // Venue name
        holder.title.setText(venue.getName());

        // Distance / Category (Address)
        holder.subTitle.setText(VenueAccessor.getSubTitle(venue));

        // Icon ( if category is existed. )
        if (0 < venue.getCategories().size()) {
            Category category = venue.getCategories().get(0);
            Icon icon = category.getIcon();
            float scale = mActivity.getResources().getDisplayMetrics().density;
            Bitmap bmp = mBmpMap.get(VenueAccessor.getSavedFileName(icon, scale, true));
            if (bmp != null) {
                holder.icon.setImageBitmap(bmp);
            } else {
                requestCategoryIcon(icon, scale, position);
            }
        } else {
            holder.icon.setImageResource(R.drawable.ic_none);
        }
//        Log.v(TAG, "getView [out]");
        return convertView;
    }

    private void requestCategoryIcon(Icon icon, float scale, int position) {
        String url = VenueAccessor.getIconUrl(icon, scale, true);
        if (url != null) {
            CategoryImageRequest request = new CategoryImageRequest(mActivity, url, new ImageRequestListener() {
                @Override
                public void onImageFetched(Bitmap bmp, String fileName) {
//                    Log.v(TAG, "onImageFetched [in] : name = " + fileName);
                    mBmpMap.put(fileName, bmp);
                    notifyDataSetChanged();
//                    Log.v(TAG, "onImageFetched [out]");
                }

                @Override
                public void onError(String errorMsg) {

                }
            });
            request.execute();
        }
    }

}

