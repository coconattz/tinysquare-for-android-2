package jp.natzsoft.tinysquare.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;

/**
 * Created by nattz on 2015/05/06.
 */

public class SelectVenueListAdapter extends ArrayAdapter<Venue> {

    private static final String TAG = "SelectVenueListAdapter";

    public class CheckViewHolder {
        TextView title;
        TextView subTitle;
        CheckBox checkBox;
    }

    private LayoutInflater mLayoutInflater;
    private MainActivity mActivity;
    private HashMap<String, Bitmap> mBmpMap;
    private ArrayList<Boolean> checkedVenues;

    public ArrayList<Boolean> getCheckedVenues() {
        return checkedVenues;
    }
    public boolean isCheckedVenues() {
        boolean ret = false;
        for (boolean checked : checkedVenues) {
            if (checked) {
                ret = true;
                break;
            }
        }
        return ret;
    }

    public SelectVenueListAdapter(Context context, int textViewResourceId, List<Venue> objects) {
        super(context, textViewResourceId, objects);
        mActivity = (MainActivity)context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mBmpMap = mActivity.getBmpMap();
        checkedVenues = new ArrayList<Boolean>(objects.size());
        for (int i = 0; i < objects.size(); i++) {
            checkedVenues.add(false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        Log.v(TAG, "getView [in] : position = " + String.valueOf(position));

        final CheckViewHolder holder;

        // 特定の行(position)のデータを得る
        Venue venue = (Venue)getItem(position);

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_select_venue, null);
            holder = new CheckViewHolder();
            holder.title = (TextView)convertView.findViewById(R.id.name);
            holder.subTitle = (TextView)convertView.findViewById(R.id.venueId);
            holder.checkBox = (CheckBox)convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);
        } else {
            holder = (CheckViewHolder)convertView.getTag();
        }

        // Venue name
        holder.title.setText(venue.getName());

        // Distance / Category (Address)
        holder.subTitle.setText(VenueAccessor.getSubTitle(venue));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkedVenues.set(position, isChecked);
                Log.v(TAG, "onCheckedChanged : p = " + position + " : check = " + isChecked);
            }
        });
        holder.checkBox.setChecked(checkedVenues.get(position));


//        Log.v(TAG, "getView [out]");
        return convertView;
    }
}
