package jp.natzsoft.tinysquare.View;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.condesales.criterias.CheckInCriteria;
import br.com.condesales.listeners.CheckInListener;
import br.com.condesales.models.Category;
import br.com.condesales.models.Checkin;
import br.com.condesales.models.Icon;
import br.com.condesales.models.Venue;

import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.MainActivity;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.Utils.ModelUtils;

/**
 * Created by natsume on 2015/03/26.
 */
public class CheckInDetailFragment extends Fragment {

    private static final String TAG = "CheckInDetailFragment";

    private static final String KEY_SHOUT = "shout";
    private static final String KEY_BROADCAST = "broadcast";
    private static final String KEY_TWITTER = "twitter";
    private static final String KEY_FACEBOOK = "facebook";
    private static final String KEY_AUTO_CHECKIN = "autoCheckin";

    private MainActivity mActivity;
    private Venue mVenue;
    private ProgressBar mProgressBar;
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    private EditText mEditText;

    private boolean mIsPrivate;
    private boolean mIsTwitter;
    private boolean mIsFacebook;
    private boolean mAutoCheckin;

    public void setVenue(Venue venue) {
        mVenue = venue;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_check_in, container, false);

        layoutSubViews(view, savedInstanceState);

        return view;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        ViewGroup viewGroup = (ViewGroup)getView();
        viewGroup.removeAllViewsInLayout();
        View view = inflater.inflate(R.layout.fragment_detail_check_in, viewGroup);
        Bundle bundle = new Bundle();
        onSaveInstanceState(bundle);
        layoutSubViews(view, bundle);
    }

    private void layoutSubViews(View view, Bundle savedInstanceState) {
        // Progress Bar
        mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);

        // Icon
        final ImageView iconView = (ImageView)view.findViewById(R.id.detailIcon);
        if (0 < mVenue.getCategories().size()) {
            Category category = mVenue.getCategories().get(0);
            Icon icon = category.getIcon();
            float scale = mActivity.getResources().getDisplayMetrics().density;
            Bitmap bmp = mActivity.getBmpMap().get(VenueAccessor.getSavedFileName(icon, scale, true));
            if (bmp != null) {
                iconView.setImageBitmap(bmp);
            } else {
                iconView.setImageResource(R.drawable.ic_none);
            }
        } else {
            iconView.setImageResource(R.drawable.ic_none);
        }

        // Venue name
        final TextView name = (TextView)view.findViewById(R.id.detailName);
        name.setText(mVenue.getName());

        // Sub title
        final TextView subTitle = (TextView)view.findViewById(R.id.detailSubTitle);
        subTitle.setText(VenueAccessor.getSubTitle(mVenue));

        // Facebook
        if (savedInstanceState != null) {
            mIsFacebook = savedInstanceState.getBoolean(KEY_FACEBOOK);
        }
        final ImageButton facebookBtn = (ImageButton)view.findViewById(R.id.detailFbButton);
        facebookBtn.setActivated(mIsFacebook);
        facebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsFacebook = !mIsFacebook;
                updateFacebookBtn(facebookBtn);
            }
        });

        // Twitter
        if (savedInstanceState != null) {
            mIsTwitter = savedInstanceState.getBoolean(KEY_TWITTER);
        }
        final ImageButton twitterBtn = (ImageButton)view.findViewById(R.id.detailTwtButton);
        twitterBtn.setActivated(mIsTwitter);
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsTwitter = !mIsTwitter;
                updateTwitterBtn(twitterBtn);
            }
        });

        // Public / Private
        if (savedInstanceState != null) {
            mIsPrivate = savedInstanceState.getBoolean(KEY_BROADCAST);
        }
        final TextView broadcastText = (TextView)view.findViewById(R.id.detailPublicText);
        final ImageButton broadcast = (ImageButton)view.findViewById(R.id.detailPublicButton);
        broadcast.setActivated(mIsPrivate);
        broadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsPrivate = !mIsPrivate;
                updateBroadcast(broadcast, broadcastText, twitterBtn, facebookBtn);
            }
        });
        updateBroadcast(broadcast, broadcastText, twitterBtn, facebookBtn);

        // Add auto-checkin list
        boolean disableCheck;
        if (ModelUtils.isLiteVer(mActivity)) {
            disableCheck = mActivity.getDBAccessor().getVenueDataDao().loadAll().size() > 0;
        } else {
            disableCheck = mActivity.isAddedGeofence(mVenue.getId());
        }
        final CheckBox autoCheckin = (CheckBox)view.findViewById(R.id.detailCheckBox);
        if (disableCheck) {
            autoCheckin.setChecked(true);
            autoCheckin.setEnabled(false);
        } else {
            if (savedInstanceState != null) {
                autoCheckin.setChecked(savedInstanceState.getBoolean(KEY_AUTO_CHECKIN));
            }
        }
        mAutoCheckin = autoCheckin.isChecked();
        autoCheckin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mAutoCheckin = isChecked;
            }
        });

        // Google Map
        mMapView = (MapView)view.findViewById(R.id.detailMapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                if (mGoogleMap != null) {
                    mGoogleMap.setMyLocationEnabled(true);
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
                    mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
                    android.location.Location location = mActivity.getLastLocation();
                    LatLng latLng = new LatLng(mVenue.getLocation().getLat(), mVenue.getLocation().getLng());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15.0f);
                    mGoogleMap.moveCamera(cameraUpdate);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mGoogleMap.addMarker(markerOptions);
                }
            }
        });

        // Shout
        mEditText = (EditText)view.findViewById(R.id.detailComment);
        if (savedInstanceState != null) {
            mEditText.setText(savedInstanceState.getString("shout"));
        }
        mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean ret = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mMapView.onResume();
                    InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    ret = true;
                }
                return ret;
            }
        });
        mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mMapView.onPause();
                } else {
                    mMapView.onResume();
                    InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        // Check-in
        final Button checkin = (Button)view.findViewById(R.id.detailCheckinButton);
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInCriteria criteria = new CheckInCriteria();
                // Venue id
                criteria.setVenueId(mVenue.getId());
                // Shout
                criteria.setShout(mEditText.getText().toString());
                // Broadcast
                criteria.setBroadcast(VenueAccessor.getBroadcastType(mIsPrivate, mIsTwitter, mIsFacebook));
                // Location
                criteria.setLocation(mActivity.getLastLocation());

                mProgressBar.setVisibility(View.VISIBLE);
                mActivity.getEasyFoursquare().checkIn(checkInListener, criteria);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_BROADCAST, mIsPrivate);
        outState.putBoolean(KEY_TWITTER, mIsTwitter);
        outState.putBoolean(KEY_FACEBOOK, mIsFacebook);
        outState.putBoolean(KEY_AUTO_CHECKIN, mAutoCheckin);
        outState.putString(KEY_SHOUT, mEditText.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        mActivity.sendScreen("チェックイン詳細");
    }

    @Override
    public void onResume() {
        mActivity.setTitle(mActivity.getString(R.string.app_name));
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void updateBroadcast(final ImageButton broadcast, final TextView broadcastText, final ImageButton twitterBtn, final ImageButton facebookBtn) {
        if (mIsPrivate) {
            broadcast.setActivated(true);
            broadcastText.setText(R.string.detail_private);
            facebookBtn.setVisibility(View.INVISIBLE);
            twitterBtn.setVisibility(View.INVISIBLE);
        } else {
            broadcast.setActivated(false);
            broadcastText.setText(R.string.detail_public);
            facebookBtn.setVisibility(View.VISIBLE);
            twitterBtn.setVisibility(View.VISIBLE);
        }
    }

    private void updateTwitterBtn(final ImageButton twitterBtn) {
        if (mIsTwitter) {
            twitterBtn.setActivated(true);
        } else {
            twitterBtn.setActivated(false);
        }
    }

    private void updateFacebookBtn(final ImageButton facebookBtn) {
        if (mIsFacebook) {
            facebookBtn.setActivated(true);
        } else {
            facebookBtn.setActivated(false);
        }
    }

    private CheckInListener checkInListener = new CheckInListener() {
        @Override
        public void onCheckInDone(Checkin checkin) {
            Log.v(TAG, "onCheckInDone [in]");
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(mActivity, R.string.complete_checkin, Toast.LENGTH_SHORT).show();

            String name = "";
            if (checkin.getVenue().getName() != null) {
                name = checkin.getVenue().getName();
            }
            if (mAutoCheckin) {
                Venue venue = checkin.getVenue();
                if (!mActivity.getDBAccessor().isExistedVenueDataById(venue.getId())) {
                    Integer broadcast = VenueAccessor.getBroadcast(mIsPrivate, mIsTwitter, mIsFacebook);
                    long rowId = mActivity.addGeofence(venue, broadcast, true);
                    mActivity.getDBAccessor().updateDate(rowId);

                    mActivity.sendEvent("Auto-Check-in", "登録", name);
                } else {
                    mActivity.getDBAccessor().updateDate(venue.getId());
                }
            }

            mActivity.sendEvent("Check-in", "手動チェックイン完了", name);

            // Remove fragment
            getFragmentManager().popBackStack();
            Log.v(TAG, "onCheckInDone [out]");
        }

        @Override
        public void onError(String errorMsg) {
            Log.v(TAG, "onError [in] : " + errorMsg);
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(mActivity, errorMsg, Toast.LENGTH_LONG).show();
            mActivity.sendEvent("Error", "Check-in", errorMsg);
            Log.v(TAG, "onError [out]");
        }
    };
}
