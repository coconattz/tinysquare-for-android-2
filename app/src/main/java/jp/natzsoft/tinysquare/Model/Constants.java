package jp.natzsoft.tinysquare.Model;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by natsume on 2015/03/30.
 */
public class Constants {

    private Constants() {
    }

    public static final String PACKAGE_NAME = "jp.natzsoft.tinysquare";

    public static final String SHARED_PREFERENCES_NAME = PACKAGE_NAME + ".SHARED_PREFERENCES_NAME";

    public static final String GEOFENCES_STARTED_KEY = PACKAGE_NAME + ".GEOFENCES_STARTED_KEY";

    public static final String ACCESS_TOKEN = "access_token";
    /**
     * Used to set an expiration time for a geofence. After this amount of time Location Services
     * stops tracking the geofence.
     */
    public static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;

    /**
     * For this sample, geofences expire after twelve hours.
     */
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS =
            GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 100; // 100m

    /**
     * Map for storing information about airports in the San Francisco bay area.
     */
    public static final HashMap<String, LatLng> BAY_AREA_LANDMARKS = new HashMap<String, LatLng>();
    static {
        // 桜木町
        BAY_AREA_LANDMARKS.put("SAKURAGI-CHO", new LatLng(35.450918, 139.631073));

        // センター南
        BAY_AREA_LANDMARKS.put("CENTER-MINAMI", new LatLng(35.545523,139.574497));

        // ドックヤードガーデン
        BAY_AREA_LANDMARKS.put("DOCKYARD-GARDEN", new LatLng(35.454954, 139.631386));
    }
}
