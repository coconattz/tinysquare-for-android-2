package jp.natzsoft.tinysquare.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;
import java.util.List;

import br.com.condesales.models.Category;
import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.db.DaoMaster;
import jp.natzsoft.tinysquare.db.DaoSession;
import jp.natzsoft.tinysquare.db.VenueCategory;
import jp.natzsoft.tinysquare.db.VenueCategoryDao;
import jp.natzsoft.tinysquare.db.VenueData;
import jp.natzsoft.tinysquare.db.VenueDataDao;

/**
 * Created by natsume on 2015/04/14.
 */
public class DBAccessor {

    private DaoSession mDaoSession;

    public DBAccessor() {

    }

    public DBAccessor(Context context) {
        initDatabase(context);
    }

    public void initDatabase(Context context) {
        SQLiteDatabase db = new DaoMaster.DevOpenHelper(context, "tinysquare-db", null).getWritableDatabase();
        mDaoSession = new DaoMaster(db).newSession();
    }

    public VenueDataDao getVenueDataDao() {
        return mDaoSession.getVenueDataDao();
    }

    public VenueCategoryDao getVenueCategoryDao() {
        return mDaoSession.getVenueCategoryDao();
    }

    public boolean isExistedVenueDataById(String id) {
        if (getVenueDataById(id) != null) {
            return true;
        }
        return false;
    }

    public VenueData getVenueDataById(String id) {
        VenueDataDao dataDao = mDaoSession.getVenueDataDao();
        List venues = dataDao.queryBuilder()
                .where(VenueDataDao.Properties.VenueId.eq(id))
                .list();
        if (venues.size() > 0) {
            return (VenueData)venues.get(0);
        }
        return null;
    }

    public VenueCategory getVenueCategoryById(String id) {
        VenueCategoryDao categoryDao = mDaoSession.getVenueCategoryDao();
        List categories = categoryDao.queryBuilder()
                .where(VenueCategoryDao.Properties.CategoryId.eq(id))
                .list();
        if (categories.size() > 0) {
            return (VenueCategory)categories.get(0);
        }
        return null;
    }

    public long addCategory(Category category) {
        VenueCategory venueCategory = new VenueCategory();
        venueCategory.setCategoryId(category.getId());
        venueCategory.setName(category.getName());
        venueCategory.setPrefix(category.getIcon().getPrefix());
        venueCategory.setSuffix(category.getIcon().getSuffix());

        return getVenueCategoryDao().insert(venueCategory);
    }

    public long addVenue(Venue venue, Integer broadcast) {

        // Check if the category is existed.
        Category category = null;
        if (0 < venue.getCategories().size()) {
            category = venue.getCategories().get(0);
        }
        VenueCategory venueCategory = null;
        if (category != null) {
            venueCategory = getVenueCategoryById(category.getId());
        }
        long rowId = 0;
        if (venueCategory == null && category != null) {
            rowId = addCategory(category);
            venueCategory = getVenueCategoryDao().loadByRowId(rowId);
        }

        // Add new VenueData
        VenueData venueData = new VenueData();
        venueData.setVenueId(venue.getId());
        venueData.setName(venue.getName());
        venueData.setLatitude(venue.getLocation().getLat());
        venueData.setLongitude(venue.getLocation().getLng());
        if (venueCategory != null) {
            venueData.setVenueCategory(venueCategory);
        }
        venueData.setBroadcast(broadcast);
        venueData.setRadius(0);
//        venueData.setLastCheckin(new Date());

        return getVenueDataDao().insert(venueData);
    }

    public void updateDate(long rowId) {
        VenueData venue = getVenueDataDao().loadByRowId(rowId);
        if (venue != null) {
            venue.setLastCheckin(new Date());
            venue.update();
        }
    }

    public void updateDate(String id) {
        VenueData venue = getVenueDataById(id);
        if (venue != null) {
            venue.setLastCheckin(new Date());
            venue.update();
        }
    }
}
