package jp.natzsoft.tinysquare.Controller;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import java.net.URL;
import java.util.Date;

import br.com.condesales.criterias.CheckInCriteria;
import br.com.condesales.models.Category;
import br.com.condesales.models.Icon;
import br.com.condesales.models.Location;
import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.R;
import jp.natzsoft.tinysquare.db.VenueData;

/**
 * Created by nattz on 2015/03/28.
 */
public class VenueAccessor {

    private static final String TAG = "VenueAccessor";

    public static final Integer BROADCAST_PUBLIC   = 0x0;
    public static final Integer BROADCAST_PRIVATE  = 0x1;
    public static final Integer BROADCAST_TWITTER  = 0x2;
    public static final Integer BROADCAST_FACEBOOK = 0x4;

    public static boolean isPrivate(Integer broadcast) {
        return (broadcast & BROADCAST_PRIVATE) != 0;
    }

    public static boolean isTwitter(Integer broadcast) {
        return (broadcast & BROADCAST_TWITTER) != 0;
    }

    public static boolean isFacebook(Integer broadcast) {
        return (broadcast & BROADCAST_FACEBOOK) != 0;
    }

    public static Integer getBroadcast(boolean privateBroadcast, boolean twitter, boolean facebook) {
        Integer ret = BROADCAST_PUBLIC;

        // Private
        if (privateBroadcast) {
            return BROADCAST_PRIVATE;
        }

        // Public / Twitter or and Facebook
        if (twitter) {
            ret |= BROADCAST_TWITTER;
        }
        if (facebook) {
            ret |= BROADCAST_FACEBOOK;
        }
        return ret;

    }

    public static Integer getBroadcast(CheckInCriteria.BroadCastType type) {
        Integer ret = BROADCAST_PUBLIC;

        if (type.equals(CheckInCriteria.BroadCastType.PRIVATE)) {
            return BROADCAST_PRIVATE;
        }

        if (type.equals(CheckInCriteria.BroadCastType.PUBLIC_TWITTER)) {
            ret |= BROADCAST_TWITTER;
        }
        if (type.equals(CheckInCriteria.BroadCastType.PUBLIC_FACEBOOK)) {
            ret |= BROADCAST_FACEBOOK;
        }
        if (type.equals(CheckInCriteria.BroadCastType.PUBLIC_TWITTER_FACEBOOK)) {
            ret |= BROADCAST_TWITTER & BROADCAST_FACEBOOK;
        }
        return ret;
    }

    public static CheckInCriteria.BroadCastType getBroadcastType(Integer broadcast) {
        // Private
        if ((broadcast & BROADCAST_PRIVATE) != 0) {
            return CheckInCriteria.BroadCastType.PRIVATE;
        }

        // Public / Twitter or and Facebook
        boolean twitter = (broadcast & BROADCAST_TWITTER) != 0;
        boolean facebook = (broadcast & BROADCAST_FACEBOOK) != 0;
        if (twitter && facebook) {
            return CheckInCriteria.BroadCastType.PUBLIC_TWITTER_FACEBOOK;
        }
        if (twitter) {
            return CheckInCriteria.BroadCastType.PUBLIC_TWITTER;
        }
        if (facebook) {
            return CheckInCriteria.BroadCastType.PUBLIC_FACEBOOK;
        }
        return CheckInCriteria.BroadCastType.PUBLIC;
    }

    public static CheckInCriteria.BroadCastType getBroadcastType(boolean privateBroadcast, boolean twitter, boolean facebook) {
        // Private
        if (privateBroadcast) {
            return CheckInCriteria.BroadCastType.PRIVATE;
        }

        // Public / Twitter or and Facebook
        if (twitter && facebook) {
            return CheckInCriteria.BroadCastType.PUBLIC_TWITTER_FACEBOOK;
        }
        if (twitter) {
            return CheckInCriteria.BroadCastType.PUBLIC_TWITTER;
        }
        if (facebook) {
            return CheckInCriteria.BroadCastType.PUBLIC_FACEBOOK;
        }
        return CheckInCriteria.BroadCastType.PUBLIC;
    }

    /**
     * Get sub title string
     * @param venue
     * @return Sub title string ( 1.2km / Park (1-2-3, Address, NY) )
     */
    public static String getSubTitle(Venue venue){
        Category category = null;
        if (0 < venue.getCategories().size()) {
            category = venue.getCategories().get(0);
        }
        Location location = venue.getLocation();
        double distance = location.getDistance();
        String distanceString;
        if (100 < distance) {
            distanceString = String.format("%.1f", distance/1000) + "km";
        } else {
            distanceString = String.valueOf((int)distance) + "m";
        }
        String subString = distanceString;
        if (category != null && category.getName() != null) {
            subString += " / " + category.getName();
        }
        if (location.getAddress() != null) {
            subString += " (" + location.getAddress() + ")";
        }
        return subString;
    }

    public static String getLastCheckin(Context context, VenueData venue) {
        String prefix = context.getString(R.string.date_last_checkin);
        Date date = venue.getLastCheckin();
        String dateText = "";
        String timeText = "";
        if (date != null) {
            dateText = DateFormat.getDateFormat(context).format(venue.getLastCheckin());
            timeText = DateFormat.getTimeFormat(context).format(venue.getLastCheckin());
        }
        return prefix + " " + dateText + " " + timeText;
    }

    /**
     * Get Icon url string
     * @param icon
     * @param scale
     * @param background
     * @return Icon url string ()
     */
    public static String getIconUrl(Icon icon, final float scale, final boolean background) {
        if (icon == null) return null;

        return icon.getPrefix() + VenueAccessor.getIconSizeString(scale, true) + icon.getSuffix();
    }

    public static String getIconUrl(final String prefix, final String suffix, final float scale, final boolean background) {
        return prefix + VenueAccessor.getIconSizeString(scale, true) + suffix;
    }

    public static String getSavedFileName(Icon icon, final float scale, final boolean background) {
        String fileName = null;
        try {
            URL url = new URL(VenueAccessor.getIconUrl(icon, scale, background));
            String path = url.getPath();
            String[] paths = path.split("/");
            fileName = paths[paths.length-2] + "_" + paths[paths.length-1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    public static String getSavedFileName(final String prefix, final String suffix, final float scale, final boolean background) {
        String fileName = null;
        try {
            URL url = new URL(VenueAccessor.getIconUrl(prefix, suffix, scale, background));
            String path = url.getPath();
            String[] paths = path.split("/");
            fileName = paths[paths.length-2] + "_" + paths[paths.length-1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    /**
     *
     * @param scale
     * @param background
     * @return
     */
    private static String getIconSizeString(final float scale, final boolean background) {
        String sizeString = "32";
        if (3.0 <= scale) {
            sizeString = "88";
        } else if (2.0 <= scale) {
            sizeString = "64";
        } else if (1.5 <= scale) {
            sizeString = "44";
        }
        if (background) {
            sizeString = "bg_" + sizeString;
        }
        return sizeString;
    }

    /**
     * Criteria
     */
    public static CheckInCriteria createCheckInCriteria(VenueData venue, android.location.Location location) {
        CheckInCriteria criteria = new CheckInCriteria();
        criteria.setVenueId(venue.getVenueId());
        if (location != null) {
            criteria.setLocation(location);
        }
        criteria.setShout(venue.getShout());
        criteria.setBroadcast(getBroadcastType(venue.getBroadcast()));
        return criteria;
    }
}
