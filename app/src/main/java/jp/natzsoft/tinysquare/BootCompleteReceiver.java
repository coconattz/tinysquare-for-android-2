package jp.natzsoft.tinysquare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by nattz on 2015/05/16.
 */
public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // Send Intent to TinysquareService
            Intent newIntent = new Intent(context, TinysquareService.class);
            newIntent.setAction(TinysquareService.INTENT_ACTION_INITIALIZE);
            context.startService(newIntent);
        }
    }
}
