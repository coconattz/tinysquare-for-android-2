package jp.natzsoft.tinysquare;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.maps.MapsInitializer;

import java.util.Date;
import java.util.HashMap;

import br.com.condesales.EasyFoursquareAsync;
import br.com.condesales.models.Venue;
import jp.natzsoft.tinysquare.Controller.LastLocationFinder;
import jp.natzsoft.tinysquare.Model.DBAccessor;
import jp.natzsoft.tinysquare.View.CheckInListFragment;
import jp.natzsoft.tinysquare.db.VenueData;


public class MainActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_PERMISSON_CODE = 1;

    // Foursquare api and listener
    private EasyFoursquareAsync async;

    // Location service
    private LastLocationFinder mLastLocationFinder;
    private Location mLastLocation;
    private LocationListener locationListener;

    private HashMap<String, Bitmap> mBmpMap;

    private ProgressBar mProgressBar;

    private SharedPreferences mSharedPreferences;

    private DBAccessor mDBAccessor;

    // Getter
    public EasyFoursquareAsync getEasyFoursquare() {
        return async;
    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    public HashMap<String, Bitmap> getBmpMap() {
        return mBmpMap;
    }

    public DBAccessor getDBAccessor() {
        return mDBAccessor;
    }

    public SharedPreferences getDefaultSharedPreferences() {
        return mSharedPreferences;
    }

    // Setter
    public void setLocationListener(LocationListener listener) {
        locationListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);

        mBmpMap = new HashMap<String, Bitmap>();
        async = new EasyFoursquareAsync(this);

        mLastLocationFinder = new LastLocationFinder(this);
        mLastLocationFinder.setChangedLocationListener(mLocationListener);

        ///////////////////////
        // Retrieve an instance of the SharedPreferences object.
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mDBAccessor = new DBAccessor();
        mDBAccessor.initDatabase(getApplicationContext());

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((MainApplication)getApplication()).getTracker();

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.v(TAG, "shouldShowRequestPermissionRationale");
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSON_CODE);
            }
        } else {
            sendTinysquareServiceInitializeIntent();

            if (savedInstanceState == null) {
                CheckInListFragment fragment = new CheckInListFragment();
                getFragmentManager().beginTransaction().add(R.id.container, fragment, "venueList").commit();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSON_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendTinysquareServiceInitializeIntent();
                    CheckInListFragment fragment = new CheckInListFragment();
                    getFragmentManager().beginTransaction().add(R.id.container, fragment, "venueList").commit();
                } else {
                    Log.v(TAG, "permission denied!");

                }
                return;
        }
    }

    /**
     * Location listener
     */
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.v(TAG, "onLocationChanged [in]");
            // Set last location.
            mLastLocation = location;
            if (locationListener != null) {
                locationListener.onLocationChanged(location);
            }
            Log.v(TAG, "onLocationChanged [out]");
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.v(TAG, "onStatusChanged [in] : string = " + s + " : i = " + i + " : bundle = " + bundle);
            if (locationListener != null) {
                locationListener.onStatusChanged(s, i, bundle);
            }
            Log.v(TAG, "onStatusChanged [out]");
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.v(TAG, "onProviderEnabled [in] : string = " + s);
            if (locationListener != null) {
                locationListener.onProviderEnabled(s);
            }
            Log.v(TAG, "onProviderEnabled [out]");
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.v(TAG, "onProviderDisabled [in] : string = " + s);
            if (locationListener != null) {
                locationListener.onProviderDisabled(s);
            }
            Log.v(TAG, "onProviderDisabled [out]");
        }
    };

    public void startUpdateLocation() {
        Log.v(TAG, "startUpdateLocation [in]");

        if (!async.hasAccessToken()) {
            return;
        }

        Date date = new Date();
        long time = date.getTime() - 60 * 1000;
        mLastLocation = mLastLocationFinder.getLastBestLocation(100, time);
        if (mLastLocation != null && locationListener != null) {
            locationListener.onLocationChanged(mLastLocation);
        }

        Log.v(TAG, "startUpdateLocation [out]");
    }

    public void stopUpdateLocation() {
        mLastLocationFinder.stop();
    }

    /**
     * Progress Bar
     */
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Add Geofence to GeofenceManager and DBAccessor
     * @param venue Venue data
     */
    public long addGeofence(Venue venue, Integer broadcast, boolean sendIntent) {
        // Add Database
        long ret = mDBAccessor.addVenue(venue, broadcast);

        if (sendIntent) {
            sendTinysquareServiceUpdateIntent();
        }
        return ret;
    }

    public void updateGeofence(VenueData venueData) {
        sendTinysquareServiceUpdateIntent();
    }

    public void removeGeofence(String requestId) {
        sendTinysquareServiceUpdateIntent();
    }

    private void sendTinysquareServiceInitializeIntent() {
        // Send Intent to TinysquareService
        Intent newIntent = new Intent(MainActivity.this, TinysquareService.class);
        newIntent.setAction(TinysquareService.INTENT_ACTION_INITIALIZE);
        startService(newIntent);
    }

    private void sendTinysquareServiceUpdateIntent() {
        // Send Intent to TinysquareService
        Intent newIntent = new Intent(MainActivity.this, TinysquareService.class);
        newIntent.setAction(TinysquareService.INTENT_ACTION_DATA_UPDATED);
        startService(newIntent);
    }

    public boolean isAddedGeofence(String id) {
        return mDBAccessor.isExistedVenueDataById(id);
    }

    /**
     * GoogleAnalytics
     */
    public void sendScreen(String name) {
        ((MainApplication)getApplication()).sendScreen(name);
    }

    public void sendEvent(final String category, final String action, final String label) {
        ((MainApplication)getApplication()).sendEvent(category, action, label);
    }
}
