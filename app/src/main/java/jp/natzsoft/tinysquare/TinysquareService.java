package jp.natzsoft.tinysquare;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationServices;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import br.com.condesales.EasyFoursquareAsync;
import br.com.condesales.criterias.CheckInCriteria;
import br.com.condesales.listeners.CheckInListener;
import br.com.condesales.models.Checkin;
import jp.natzsoft.tinysquare.Controller.VenueAccessor;
import jp.natzsoft.tinysquare.Geofence.GeofenceManager;
import jp.natzsoft.tinysquare.Model.Constants;
import jp.natzsoft.tinysquare.Model.DBAccessor;
import jp.natzsoft.tinysquare.db.VenueData;

/**
 * Created by nattz on 2015/04/26.
 */
public class TinysquareService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "TinysquareService";

    public static final String INTENT_ACTION_INITIALIZE = "intent.action.INITIALIZE";
    public static final String INTENT_ACTION_DATA_UPDATED = "intent.action.DATA_UPDATED";
    public static final String INTENT_ACTION_ON_OFF_GEOFENCE = "intent.action.ON_OFF_GEOFENCE";
    public static final String INTENT_ACTION_ENTER_EXIT_GEOFENCE = "intent.action.ENTER_EXIT_GEOFENCE";

    private static final String PREFERENCE_KEY_TIME_TO_CHECK_IN = "key_list_time_to_check_in";
    private static final String PREFERENCE_KEY_NEXT_CHECK_IN_TIME = "key_list_next_check_in_time";

    private Context mContext;
    ConcurrentHashMap<String, CountDownTimer> mTimerMap;

    protected GoogleApiClient mGoogleApiClient;
    protected GeofenceManager mGeofenceManager;
    protected String mAction;

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate [in]");
        super.onCreate();

        mTimerMap = new ConcurrentHashMap<String, CountDownTimer>();
        // Get the MainActivity context.
        mContext = getApplicationContext();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        Log.v(TAG, "onCreate [out]");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand [in] start id " + startId + ": " + intent);

        if (intent == null) {
            Log.e(TAG, "intent is null !");
            return statusService();
        }

        String action = intent.getAction();
        mAction = action;
        if (action.equals(INTENT_ACTION_INITIALIZE)) {
            Log.v(TAG, "INTENT_ACTION_INITIALIZE");
            mGeofenceManager.initializeGeofences();
        } else if (action.equals(INTENT_ACTION_DATA_UPDATED)) {
            Log.v(TAG, "INTENT_ACTION_DATA_UPDATED");
            mGeofenceManager.updateAllGeofences();
        } else if (action.equals(INTENT_ACTION_ON_OFF_GEOFENCE)) {
            Log.v(TAG, "INTENT_ACTION_ON_OFF_GEOFENCE");
            mGeofenceManager.updateGeofenceService(intent.getBooleanExtra("enabled", false));
        } else if (action.equals(INTENT_ACTION_ENTER_EXIT_GEOFENCE)) {
            Log.v(TAG, "INTENT_ACTION_ENTER_EXIT_GEOFENCE");
            enterExitGeofence(intent);
        }
        Log.v(TAG, "onStartCommand [out]");
        return statusService();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy [in]");
        super.onDestroy();
        mGoogleApiClient.disconnect();
        Log.v(TAG, "onDestroy [out]");
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGeofenceManager = new GeofenceManager(mContext, mGoogleApiClient);
        mGeofenceManager.setGeofenceManagerListener(new GeofenceManager.GeofenceManagerListener() {
            @Override
            public void updateGeofenceService(final boolean start) {
                if (mAction != null &&
                        (mAction.equals(INTENT_ACTION_INITIALIZE) || mAction.equals(INTENT_ACTION_ON_OFF_GEOFENCE))) {
                    sendNotification(mContext.getString(R.string.app_name),
                            mContext.getString(start ? R.string.geofences_started : R.string.geofences_stopped));
                }
            }
        });
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        mGeofenceManager.updateGeofenceService();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    private synchronized void checkStopService() {
        if (statusService() == START_STICKY) {
            stopSelf();
        }
    }

    private int statusService() {
        int status = START_NOT_STICKY;
        if (mTimerMap.size() == 0) {
            status = START_STICKY;
        }
        return status;
    }

    private void enterExitGeofence(Intent intent) {
        if (mContext != null) {
            // Get the Foursquare access token.
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            final String accessToken = preferences.getString(Constants.ACCESS_TOKEN, "");

            if (accessToken != null) {
                String next_check_in_time = preferences.getString(PREFERENCE_KEY_NEXT_CHECK_IN_TIME, "10");
                String time_to_check_in = preferences.getString(PREFERENCE_KEY_TIME_TO_CHECK_IN, "1");
                final int nextCheckInTime = Integer.parseInt(next_check_in_time);
                final int timeToCheckIn = Integer.parseInt(time_to_check_in);

                final DBAccessor dbAccessor = new DBAccessor(mContext);
                final String id = intent.getStringExtra("id");
                final VenueData venue = dbAccessor.getVenueDataById(id);
                final Location location = intentLocation(intent);
                long meters = meters(location, venue);

                int geofenceTransition = intent.getIntExtra("transition", -1);
                if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {

                    if (mTimerMap.get(id) != null) {
                        Log.v(TAG, "Already existed timer.");
                        return;
                    }

                    if (timeToCheckIn > 0) {
                        CountDownTimer timer = new CountDownTimer(timeToCheckIn * 60 * 1000, timeToCheckIn * 60 * 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                checkin(venue, location, accessToken, nextCheckInTime * 60 * 60 * 1000);
//                                sendNotification(venue.getName(), "Check-In status after " + timeToCheckIn + " min.");
                            }
                        }.start();
                        mTimerMap.put(id, timer);
                    } else {
                        checkin(venue, location, accessToken, nextCheckInTime * 60 * 60 * 1000);
                    }

                    ((MainApplication)getApplication()).sendEvent("Check-in", "領域侵入", venue.getName());

//                    sendNotification(venue.getName(), "Entered (" + meters + "m)");
                } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    CountDownTimer timer = mTimerMap.get(id);
                    if (timer != null) {
                        timer.cancel();
                        mTimerMap.remove(id);
                        checkStopService();
                    }

                    ((MainApplication)getApplication()).sendEvent("Check-in", "領域撤退", venue.getName());

//                    sendNotification(venue.getName(), "Exited (" + meters + "m)");
                }

            }
        }
    }

    private Location intentLocation(Intent intent) {
        final Location location = new Location("");
        location.setLatitude(intent.getDoubleExtra("latitude", 0));
        location.setLongitude(intent.getDoubleExtra("longitude", 0));
        return location;
    }

    private long meters(Location location, VenueData venue) {
        Location venueLocation = new Location("");
        venueLocation.setLatitude(venue.getLatitude());
        venueLocation.setLongitude(venue.getLongitude());
        return  (long)venueLocation.distanceTo(location);
    }

    synchronized private void checkin(final VenueData venue, Location location, String accessToken, long nextCheckInTime) {
        Log.v(TAG, "checkin [in] venue = " + venue.getName() + "time = " + nextCheckInTime);
        if (venue != null) {
            // Check next check-in time.
            Date date = venue.getLastCheckin();
            long time = 0;
            if (date != null) {
                time = date.getTime();
            }
            Date nowDate = new Date();
            Long diffTime = nowDate.getTime() - time;
            if (diffTime < nextCheckInTime) {
                Log.v(TAG, "Too early to check-in. diffTime : " + diffTime + " nextTime : " + nextCheckInTime);
                ((MainApplication)getApplication()).sendEvent("Check-in", "経過時間未達", venue.getName());
                return;
            }

            CheckInCriteria criteria = VenueAccessor.createCheckInCriteria(venue, location);
            EasyFoursquareAsync async = new EasyFoursquareAsync(null);
            async.checkIn(new CheckInListener() {
                @Override
                public void onCheckInDone(Checkin checkin) {
                    String name = checkin.getVenue().getName();

                    Log.v(TAG, "checkin#onCheckInDone : " + name);

                    sendNotification(name, mContext.getString(R.string.complete_checkin));
                    venue.setLastCheckin(new Date());
                    venue.update();

                    mTimerMap.remove(venue.getVenueId());

                    ((MainApplication)getApplication()).sendEvent("Check-in", "自動チェックイン完了", venue.getName());

                    checkStopService();
                }

                @Override
                public void onError(String errorMsg) {
                    Log.e(TAG, "checkin#onError error : " + errorMsg);

                    mTimerMap.remove(venue.getVenueId());

                    ((MainApplication)getApplication()).sendEvent("Error", "Check-in", errorMsg);

                    checkStopService();
                }
            }, criteria, accessToken);
        }
        Log.v(TAG, "checkin [out]");
    }

    static int notificationId = 0;
    /**
     * Posts a notification in the notification bar when a transition is detected.
     * If the user clicks the notification, control goes to the MainActivity.
     */
    private void sendNotification(String title, String text) {
        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        Notification.Builder builder = new Notification.Builder(this);

        // Define the notification settings.
        builder.setContentIntent(notificationPendingIntent)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(title)
                .setContentText(text);

        // Set color (API 21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setColor(mContext.getResources().getColor(R.color.primary))
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setCategory(Notification.CATEGORY_SERVICE);
        }

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification
        mNotificationManager.notify(notificationId, builder.build());
    }
}
