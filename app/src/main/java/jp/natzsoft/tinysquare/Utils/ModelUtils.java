package jp.natzsoft.tinysquare.Utils;

import android.content.Context;

import jp.natzsoft.tinysquare.R;

/**
 * Created by natsume on 2015/05/21.
 */
public class ModelUtils {
    public static boolean isLiteVer(Context context) {
        String appName = context.getString(R.string.app_name);
        if (appName.contains("Lite")) {
            return true;
        }
        return false;
    }
}
