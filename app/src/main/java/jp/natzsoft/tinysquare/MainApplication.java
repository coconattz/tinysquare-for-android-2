package jp.natzsoft.tinysquare;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by nattz on 2015/05/20.
 */
public class MainApplication extends Application {

    private Tracker mTracker;

    synchronized Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.app_tracker);
        }
        return mTracker;
    }

    public void sendScreen(String name) {
        Tracker tracker = getTracker();
        tracker.setScreenName(name);
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    public void sendEvent(final String category, final String action, final String label) {
        getTracker().send(new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .setLabel(label)
                        .build()
        );
    }
}
