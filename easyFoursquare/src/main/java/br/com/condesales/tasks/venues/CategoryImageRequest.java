package br.com.condesales.tasks.venues;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import br.com.condesales.listeners.ImageRequestListener;

public class CategoryImageRequest extends AsyncTask<String, Integer, Bitmap> {

    private static final String TAG = "CategoryImageRequest";

	private ImageRequestListener mListener;
	private Activity mActivity;
    private String mUrlString;
    private String mFileName;
    private String errString = null;

	public CategoryImageRequest(Activity activity, String urlString, ImageRequestListener listener) {
		mListener = listener;
        mUrlString = urlString;
		mActivity = activity;
	}

	public CategoryImageRequest(Activity activity, String urlString) {
        mUrlString = urlString;
		mActivity = activity;
	}

	@Override
	protected Bitmap doInBackground(String... params) {

		Bitmap bmp = getFileInCache();
        if (bmp != null) {
            return bmp;
        }

		InputStream is = null;
		BufferedInputStream bis = null;
		try {
			URL url = new URL(mUrlString); // you can write here any link
			/* Open a connection to that URL. */
			URLConnection ucon = url.openConnection();
			/*
			 * Define InputStreams to read from the URLConnection.
			 */
			is = ucon.getInputStream();
			bis = new BufferedInputStream(is);
            saveImageInCache(bis);
			bmp = BitmapFactory.decodeStream(bis);
		} catch (Exception e) {
			Log.e("Getting user image", e.toString());
			errString = e.toString();
		} finally {
			try {
                if (is != null) {
                    is.close();
                }
                if (bis != null) {
                    bis.close();
                }
            } catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bmp;
	}

	@Override
	protected void onPostExecute(Bitmap bmp) {
		if (mListener != null)
			if (errString != null) {
				mListener.onError(errString);
			} else {
				mListener.onImageFetched(bmp, getSavedFileName(mUrlString));
			}
		super.onPostExecute(bmp);
	}

    /**
     * Saves the file into device cache memory.
     *
     * @param bufferedInputStream
     *            Input stream from url
     */
    private void saveImageInCache(BufferedInputStream bufferedInputStream) {
        if (bufferedInputStream != null) {
            String path = mActivity.getCacheDir().toString();
            OutputStream fOut = null;
            File file = new File(path, getSavedFileName(mUrlString));
            try {
                fOut = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                errString = e.toString();
            }
            try {
                byte[] bytes = new byte[1024];
                while ((bufferedInputStream.read(bytes)) >= 0) {
                   fOut.write(bytes);
                }
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                errString = e.toString();
            } catch (NullPointerException e) {
                e.printStackTrace();
                errString = e.toString();
            }
        }
    }

    private String getSavedFileName(String urlString) {
        String fileName = null;
        try {
            URL url = new URL(urlString);
            String path = url.getPath();
            String[] paths = path.split("/");
            fileName = paths[paths.length-2] + "_" + paths[paths.length-1];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }
	/**
	 * Verifies if file already exits.
	 * 
	 * @return Bitmap containing the image. Null if file does not exists
	 */
	public Bitmap getFileInCache() {
		File[] list = mActivity.getCacheDir().listFiles();
		for (int i = 0; i < list.length; i++) {
            String[] paths = list[i].getName().split("/");
            String name = paths[paths.length-1];
			if (name.equals(getSavedFileName(mUrlString))) {
				return BitmapFactory.decodeFile(list[i].getAbsolutePath());
			}
		}
		return null;
	}
}
