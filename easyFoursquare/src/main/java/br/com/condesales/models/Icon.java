package br.com.condesales.models;

import android.app.Activity;
import android.graphics.Bitmap;

import br.com.condesales.tasks.venues.CategoryImageRequest;

public class Icon {

	private String prefix;

	private String suffix;

	public String getSuffix() {
		return suffix;
	}

	public String getPrefix() {
		return prefix;
	}

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
