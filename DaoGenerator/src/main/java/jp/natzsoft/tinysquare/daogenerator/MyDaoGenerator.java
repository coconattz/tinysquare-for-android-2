package jp.natzsoft.tinysquare.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(3, "jp.natzsoft.tinysquare.db");

        Entity venue = schema.addEntity("VenueData");
        venue.addIdProperty().autoincrement();
        venue.addStringProperty("venueId").notNull();
        venue.addStringProperty("name").notNull();
        venue.addStringProperty("shout");
        venue.addIntProperty("broadcast");

        venue.addDoubleProperty("latitude");
        venue.addDoubleProperty("longitude");
        venue.addIntProperty("radius");

        venue.addDateProperty("lastCheckin");

        Entity category = schema.addEntity("VenueCategory");
        category.addIdProperty().autoincrement();
        category.addStringProperty("categoryId").notNull();
        category.addStringProperty("name").notNull();
        category.addStringProperty("prefix");
        category.addStringProperty("suffix");

        Property categoryId = venue.addLongProperty("categoryId").notNull().getProperty();
        venue.addToOne(category, categoryId);

        ToMany venues = category.addToMany(venue, categoryId);
        venues.setName("venues");

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
